BunnyCrash
==========

Demonstrating an intermittent segfault in an OpenFL project on Linux after a Tile is added to a Tilemap when the following conditions are fulfilled:
  - The Tilemap has previously had Tiles, since removed
  - The Tilemap has been empty for a while (minutes, sometimes hours)
  - A new Tile is added to the empty Tilemap after this long pause

The crash does not always happen, but the longer the program is left running, the more likely it is to crash when the new Tile is added.

The software and hardware this has been observed on are:
 - AMD Desktop with nvidia graphics card and Intel laptop with Intel on-board graphics
 - Linux kernel versions 4.15.9-1-ARCH and 4.15.3-2-ARCH
 - Haxe compiler versions 3.4.7 and 3.4.4
 - hxcpp version 3.4.188
 - lime versions 6.2.0 and 5.9.1
 - openfl versions 7.1.2 and 6.5.3

Workaround
----------
One workaround is very simple - every Tilemap has a Tile added when it is first created, which remains out of sight (with appropriately chosen negative co-ordinates) for the lifetime of the Tilemap. In my testing so far this appears to prevent the crash.

This code is provided to document the issue and help anyone else encountering strange unpredictable crashes when adding Tiles to Tilemaps in OpenFL projects.


